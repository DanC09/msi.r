# README #

MSI.R scripts for mass spectrometry imaging (MSI) .imzML files.

### Installation ###

You can modify and run the scripts in R http://www.r-project.org/. For editing, the use of RStudio is recommended http://www.rstudio.com/

### Contribution guidelines ###

Please report your experiences and problems.

### License ###

GNU General Public License, version 3 (http://gplv3.fsf.org/).

### Contact ###

robert.winkler@ira.cinvestav.mx,
robert.winkler@bioprocess.org